<?php

require_once 'Conexion.php';
session_start();

$consulta=$pdo->query("SELECT `usuario`.`nombre`, `prestamo`.`id_articulo`, `prestamo`.`fecha_pre`, `prestamo`.`fecha_dev` FROM `prestamos`.`usuario`, `prestamos`.`prestamo` WHERE `usuario`.`id_usuario` = `prestamo`.`id_usuario`");

#$idEmpleado=$_SESSION['idEmpleado'];
$idEmpleado =  serialize($_SESSION['idEmpleado']);
$resultado=false;

if (!empty($_POST)){

  $idUsuario = $_POST['idUsuario'];
  $fechaDev = $_POST['fechaDev'];

  print_r($_POST);
  echo $idEmpleado;
  
  #if($_POST['hdmi'] != "" && $_POST['vga']  != "" && $_POST['sonido']  != ""){
  if(isset($_POST['hdmi']) && isset($_POST['vga']) && isset($_POST['sonido'])){

    $hdmi=$_POST['hdmi'];
    $vga=$_POST['vga'];
    $sonido=$_POST['sonido'];

    $sql= "INSERT INTO `prestamos`.`prestamo` (`id_usuario`,`id_articulo`,`id_empleado`,`fecha_pre`,`fecha_dev`) VALUES (:idUsuario,:hdmi,:idEmpleado,now(),:fechaDev),(:idUsuario,:vga,:idEmpleado,now(),:fechaDev),(:idUsuario,:sonido,:idEmpleado,now(),:fechaDev)";
    $query=$pdo->prepare($sql);

    $resultado=$query->execute([
      'idUsuario'=>$idUsuario,
      'hdmi'=>$hdmi,
      'vga'=>$vga,
      'sonido'=>$sonido,
      'idEmpleado'=>$idEmpleado,
      'fechaDev'=>$fechaDev
    ]);
    
  }
  elseif(isset($_POST['hdmi']) && isset($_POST['vga'])){

    $hdmi=$_POST['hdmi'];
    $vga=$_POST['vga'];

    $sql= "INSERT INTO `prestamos`.`prestamo` (`id_usuario`,`id_articulo`,`id_empleado`,`fecha_pre`,`fecha_dev`) VALUES (:idUsuario,:hdmi,:idEmpleado,now(),:fechaDev),(:idUsuario,:vga,:idEmpleado,now(),:fechaDev)";
    $query=$pdo->prepare($sql);

    $resultado=$query->execute([
      'idUsuario'=>$idUsuario,
      'hdmi'=>$hdmi,
      'vga'=>$vga,
      'idEmpleado'=>$idEmpleado,
      'fechaDev'=>$fechaDev
    ]);
    
  }
  elseif(isset($_POST['vga']) && isset($_POST['sonido'])){

    $vga=$_POST['vga'];
    $sonido=$_POST['sonido'];

    $sql= "INSERT INTO `prestamos`.`prestamo` (`id_usuario`,`id_articulo`,`id_empleado`,`fecha_pre`,`fecha_dev`) VALUES (:idUsuario,:vga,:idEmpleado,now(),:fechaDev),(:idUsuario,:sonido,:idEmpleado,now(),:fechaDev)";
    $query=$pdo->prepare($sql);

    $resultado=$query->execute([
      'idUsuario'=>$idUsuario,
      'vga'=>$vga,
      'sonido'=>$sonido,
      'idEmpleado'=>$idEmpleado,
      'fechaDev'=>$fechaDev
    ]);
    
  }
  elseif(isset($_POST['hdmi']) && isset($_POST['sonido'])){

    $hdmi=$_POST['hdmi'];
    $sonido=$_POST['sonido'];

    $sql= "INSERT INTO `prestamos`.`prestamo` (`id_usuario`,`id_articulo`,`id_empleado`,`fecha_pre`,`fecha_dev`) VALUES (:idUsuario,:hdmi,:idEmpleado,now(),:fechaDev),(:idUsuario,:sonido,:idEmpleado,now(),:fechaDev)";
    $query=$pdo->prepare($sql);

    $resultado=$query->execute([
      'idUsuario'=>$idUsuario,
      'hdmi'=>$hdmi,
      'sonido'=>$sonido,
      'idEmpleado'=>$idEmpleado,
      'fechaDev'=>$fechaDev
    ]);
    
  }
  elseif(isset($_POST['hdmi'])){

    $hdmi=$_POST['hdmi'];

    $sql= "INSERT INTO `prestamos`.`prestamo` (`id_usuario`,`id_articulo`,`id_empleado`,`fecha_pre`,`fecha_dev`) VALUES (:idUsuario,:hdmi,:idEmpleado,now(),:fechaDev)";
    $query=$pdo->prepare($sql);

    $resultado=$query->execute([
      'idUsuario'=>$idUsuario,
      'hdmi'=>$hdmi,
      'idEmpleado'=>$idEmpleado,
      'fechaDev'=>$fechaDev
    ]);
  }
  elseif(isset($_POST['vga'])){

    $vga=$_POST['vga'];

    $sql= "INSERT INTO `prestamos`.`prestamo` (`id_usuario`,`id_articulo`,`id_empleado`,`fecha_pre`,`fecha_dev`) VALUES (:idUsuario,:vga,:idEmpleado,now(),:fechaDev)";
    $query=$pdo->prepare($sql);

    $resultado=$query->execute([
      'idUsuario'=>$idUsuario,
      'vga'=>$vga,
      'idEmpleado'=>$idEmpleado,
      'fechaDev'=>$fechaDev
    ]);
  }
  else{

    $sonido=$_POST['sonido'];

    $sql= "INSERT INTO `prestamos`.`prestamo` (`id_usuario`,`id_articulo`,`id_empleado`,`fecha_pre`,`fecha_dev`) VALUES (:idUsuario,:sonido,:idEmpleado,now(),:fechaDev)";
    $query=$pdo->prepare($sql);

    $resultado=$query->execute([
      'idUsuario'=>$idUsuario,
      'sonido'=>$sonido,
      'idEmpleado'=>$idEmpleado,
      'fechaDev'=>$fechaDev
    ]);
  }
    header("Location:../registrar_prestamo.php");   
}
?>