$ (document).ready(function (){

          //Buscar Usuario
         $('#codigo').keyup(function(e){
                 e.preventDefault();
                 var us = $(this).val();
                 var action = 'searchUser';
                 $.ajax({
                    url:'ajax_buscar_usuario.php',
                    type:'POST',
                    async:true,
                    data: {action:action,usuario:us },
                    
                    success: function(response)
                        {
                          //console.log(response);
                          if(response == 0)
                          {
                            $("#idUsuario").val('');
                            $('#nombreUsuario').val('');
                            $('#identificacion').val('');
                            $('#correoUsuario').val('');
                            $('#rol').val('');
                            
                          }
                          else
                          {
                            var data = $.parseJSON(response);
                            $("#idUsuario").val(data.id_usuario);
                            $('#nombreUsuario').val(data.nombre);
                            $('#identificacion').val(data.identificacion);
                            $('#correoUsuario').val(data.correo);
                            $('#rol').val(data.rol);

                          }
                        },
                    error:function(error)
                        {
                          console.log(error);
                        }
                  });
          })
          
        
 });

   
