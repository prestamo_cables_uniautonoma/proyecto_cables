<?php
require_once("control/ctrl_registrar_prestamo.php");

#session_start();
#$varsesion=$_SESSION['usuario'];
?>

<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

  <title>Panel de control</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="./css/style.css">
  <link rel="icon" href="img/logo.ico">
</head>

<body>
  <div class="d-flex">
    <div id="sidebar" >
      <div class="p-2">
        <a href="#" class="navbar-brand text-center text-light w-100 p-4 ">
          PANEL DE CONTROL
        </a>
        <div class="logo-dashboard">
          <img src="./img/logo.png" alt="">
          <p class="text-light  text-center border-bottom">Administrador</p>
        </div>
        
      </div>
      <div id="sidebar-accordion" class="accordion">
        <div class="list-group">
            <a href="dashboard.html" class="list-group-item list-group-item-action bg-dark text-light">
                <i class="fa fa-tachometer mr-3" aria-hidden="true"></i>Dashboard
            </a>
            <a href="registrar_prestamo.php" class="list-group-item list-group-item-action bg-blue-uniautonoma text-light">
                <i class="fa fa-list-ol mr-3" aria-hidden="true"></i>Registrar Prestamo
            </a>
            <a href="registrar_item.php" class="list-group-item list-group-item-action bg-dark text-light">
                <i class="fa fa-shopping-cart mr-3 " aria-hidden="true"></i>Inventario
            </a>
          <a href="#profile-items" data-toggle="collapse" aria-expanded="false"
            class="list-group-item list-group-item-action bg-dark text-light">
            <i class="fa fa-user mr-3" aria-hidden="true"></i>Perfil
          </a>
          <div id="profile-items" class="collapse" data-parent="#sidebar-accordion">
            <a href="#" class="list-group-item list-group-item-action bg-dark text-light pl-5">
              Información
            </a>
            <a href="#" class="list-group-item list-group-item-action bg-dark text-light pl-5">
              Actualizar Datos
            </a>
          </div>
          <a href="#setting-items" data-toggle="collapse" aria-expanded="false"
            class="list-group-item list-group-item-action bg-dark text-light">
            <i class="fa fa-cog mr-3" aria-hidden="true"></i>Configuración
          </a>
          <div id="setting-items" class="collapse" data-parent="#sidebar-accordion">
            <div class="d-flex flex-row text-center">
              <a href="#" class="list-group-item list-group-item-action bg-dark text-light">
                Item 1
              </a>
              <a href="#" class="list-group-item list-group-item-action bg-dark text-light">
                Item 2
              </a>
            </div>
          </div>
          <a href="index.php" data-toggle="collapse" aria-expanded="false"
            class="list-group-item list-group-item-action bg-dark text-light">
            <i class="fa fa-sign-out mr-3" aria-hidden="true"></i>Cerrar Sesión
          </a>
        </div>
      </div>
    </div>
    <div class="content w-100">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-xl">
          <a class="navbar-brand" href="#"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07XL" aria-controls="navbarsExregistrar_prestamo.phpample07XL" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
      </nav>
      <!-- CONTENIDO -->
      <section class="p-1">
        <div class="container">
          <div class="row columna-1">
            <div class="col order-1">
              <!-- FORMULARIO -->
              <form action="control/ctrl_registrar_prestamo.php" method="post" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-7">
                        <h3 for="codigo" class="my-2">Buscar Usuario</h3>
                        <input type="search" id="codigo" name="buscarUsuario" placeholder="Buscar por número de documento" class="form-control buscador-usuario" autofocus>
                    </div>
                </div>
                <div>
                  <input type="hidden" id="idUsuario" name="idUsuario">
                </div>
                

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for=""><strong>Nombre:</strong></label>
                    <input type="text" id="nombreUsuario" name="nombreUsuario" placeholder="Ingrese nombre de usuario" class="form-control" required>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for=""><strong>Identificacion:</strong></label>
                    <input type="text" id="identificacion" name="identificacion" placeholder="Ingrese la identificacion" class="form-control" required>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for=""><strong>Correo:</strong></label>
                    <input type="email" id="correoUsuario" name="correoUsuario" placeholder="Ingrese correo institucional" class="form-control" required>
                  </div>
                </div>
                
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="rol"><strong>Rol:</strong></label>
                    <select name="rol" id="rol" class="form-control" required>
                        <option value="" selected>Seleccione</option>
                        <option value="Estudiante">Estudiante</option>
                        <option value="Docente">Docente</option>
                        <option value="Administrativo">Administrativo</option>
                    </select>
                  </div>
                </div>
                <br>
                <label class=""><strong>Seleccione el articulo:</strong></label>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="1" id="hdmi" name="hdmi">
                  <label class="form-check-label" for="">
                    HDMI
                  </label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="2" id="vga" name="vga">
                  <label class="form-check-label" for="">
                    VGA
                  </label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="3" id="sonido" name="sonido">
                  <label class="form-check-label" for="">
                    SONIDO
                  </label>
                </div>
                <br>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for=""><strong>Fecha de Devolucion:</strong></label>
                    <input type="date" id="fechaDev" name="fechaDev" class="form-control" required>
                  </div>
                </div>
                <!--
                <div class="form-row">

                    <label for=""><strong>Observaciones:</strong></label>
                    <textarea id="observacion" name="observacion"  rows="5" cols="25"></textarea>
                </div> 
                -->
                  <br>
                  <br>                
                <!-- cierre de form-row -->
                <!-- dejo fuera a submit -->
                <div class="form-group">
                    <input type="submit" value="Registrar" class="btn btn-success">
                </div>
              </form>
               <!---FIN FORMULARIO -->
            </div>
            <div class="col order-2 col-md-6">
              <h2>Historial de prestamos</h2>
              <!-- TABLA -->
              <div class="table-responsive" >
                <table class="table table-hover">
                  <thead class="table-dark">
                        <tr>
                            <th>Usuario</th>
                            <th>Articulo</th>
                            <th>Fecha prestamo</th>
                            <th>Fecha devolución</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                          while($fila=$consulta->fetch(PDO::FETCH_ASSOC)){
                            
                              echo '<div class="col-md-3">';
                                echo '<div class="member">';
                            
                                  echo '<tr>';
                                  echo '<td>'.$fila['nombre'].'</td>';
                                  if($fila['id_articulo']==1){
                                    echo '<td> HDMI </td>';
                                  }elseif($fila['id_articulo']==2){
                                    echo '<td> VGA </td>';
                                  }else{
                                    echo '<td> SONIDO </td>';
                                  }
                                  
                                  echo '<td>'.$fila['fecha_pre'].'</td>';
                                  echo '<td>'.$fila['fecha_dev'].'</td>';
                                  echo '</tr>';
                          }
                          ?> 
                    </tbody>
                </table>
              </div>
              <!---FIN TABLA -->
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
    integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
  </script>
  <script type="text/javascript" src="js/funciones.js"></script>
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
</body>

</html>