<?php
require_once("control/ctrl_registrar_item.php");

?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

  <title>Panel de control</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="./css/style.css">
  <link rel="icon" href="img/logo.ico">
</head>

<body>
  <div class="d-flex">
    <div id="sidebar" >
      <div class="p-2">
        <a href="#" class="navbar-brand text-center text-light w-100 p-4 ">
          PANEL DE CONTROL
        </a>
        <div class="logo-dashboard">
          <img src="./img/logo.png" alt="">
          <p class="text-light  text-center border-bottom">Administrador</p>
        </div>
        
      </div>
      <div id="sidebar-accordion" class="accordion">
        <div class="list-group">
          <a href="dashboard.html" class="list-group-item list-group-item-action bg-dark text-light">
            <i class="fa fa-tachometer mr-3" aria-hidden="true"></i>Dashboard
          </a>
          <a href="registrar_prestamo.php" class="list-group-item list-group-item-action bg-dark text-light">
            <i class="fa fa-list-ol mr-3" aria-hidden="true"></i>Registrar Prestamo
          </a>
          <a href="registrar_item.php" class="list-group-item list-group-item-action bg-blue-uniautonoma text-light">
            <i class="fa fa-shopping-cart mr-3 " aria-hidden="true"></i>Inventario
          </a>
          <a href="#profile-items" data-toggle="collapse" aria-expanded="false"
            class="list-group-item list-group-item-action bg-dark text-light">
            <i class="fa fa-user mr-3" aria-hidden="true"></i>Perfil
          </a>
          <div id="profile-items" class="collapse" data-parent="#sidebar-accordion">
            <a href="#" class="list-group-item list-group-item-action bg-dark text-light pl-5">
              Información
            </a>
            <a href="#" class="list-group-item list-group-item-action bg-dark text-light pl-5">
              Actualizar Datos
            </a>
          </div>
          <a href="#setting-items" data-toggle="collapse" aria-expanded="false"
            class="list-group-item list-group-item-action bg-dark text-light">
            <i class="fa fa-cog mr-3" aria-hidden="true"></i>Configuración
          </a>
          <div id="setting-items" class="collapse" data-parent="#sidebar-accordion">
            <div class="d-flex flex-row text-center">
              <a href="#" class="list-group-item list-group-item-action bg-dark text-light">
                Item 1
              </a>
              <a href="#" class="list-group-item list-group-item-action bg-dark text-light">
                Item 2
              </a>
            </div>
          </div>
          <a href="index.php" data-toggle="collapse" aria-expanded="false"
            class="list-group-item list-group-item-action bg-dark text-light">
            <i class="fa fa-sign-out mr-3" aria-hidden="true"></i>Cerrar Sesión
          </a>
        </div>
      </div>
    </div>
    <div class="content w-100">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-xl">
          <a class="navbar-brand" href="#"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
      </nav>
      <!-- CONTENIDO -->
      <section class="p-1">
              <div class="container">
                <div class="row">
                  <div class="col order-1">
                    <h2>Registrar Articulo</h2>
                    <!-- FORMULARIO -->
                    
                    <form action="control/ctrl_registrar_item.php" method="post" enctype="multipart/form-data">
                      <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="articulo">Articulo</label>
                            <select name="articulo" id="articulo" class="form-control">
                                <option value="" selected>Seleccione</option>
                                <option value="HDMI">HDMI</option>
                                <option value="VGA">VGA</option>
                                <option value="SONIDO">SONIDO</option>                            
                            </select>
                          </div>
                      </div>
                      
                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="Cantidad">Cantidad</label>
                          <input type="number" id="cantidad" name="cantidad" min="1" max="100" class="form-control">
                        </div>
                      </div>
                      <div class="form-row">
                          <div class="form-group col-md-4">
                          <label for="unidad_medida">Unidad de medida</label>
                            <select name="unidad_medida" id="unidad_medida" class="form-control">
                                <option value="" selected>Seleccione</option>
                                <option value="METROS">METRO</option>
                                <option value="CENTIMETROS">CENTIMETRO</option>
                            </select>
                          </div>  
                          <div class="form-group col-md-2">
                          <label for="longitud">longitud</label>
                            <input type="number" id="longitud" name="longitud" min="1" max="100" class="form-control">                            
                          </div>
                      </div>
              
                        

                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="Descripcion">Descripcion</label>
                          <textarea name="observacion" id="observacion" rows="3" cols="20" maxlength="200"class="form-control" placeholder="Descripcion"></textarea>
                        </div>
                      </div>
                      <!-- cierre de form-row -->
                      <!-- dejo fuera a submit -->
                      <div class="form-group">
                          <input type="submit" class="btn btn-success">
                      </div>
                    </form>
                     <!---FIN FORMULARIO -->
                  </div>
                  <div class="col order-2 col-md-7">
                    <h2>Articulos en Inventario</h2>
                    <!-- TABLA -->
                    <div class="table-responsive" >

                      <table class="table table-hover">
                        <thead class="table-dark">
                              <tr>
                                  <th>N°</th>
                                  <th>Articulo</th>
                                  <th>Cantidad</th>
                                  <th scope="col">Medida</th>
                                  <th>Descripción</th>
                                  <th>Fecha de Registro</th>
                              </tr>
                          </thead>
                          <tbody>
                          <?php
                          while($fila=$consulta->fetch(PDO::FETCH_ASSOC)){
                            
                              echo '<div class="col-md-3">';
                                echo '<div class="member">';
                            
                                  echo '<tr>';
                                  echo '<td>'.$fila['id_articulo'].'</td>';
                                  echo '<td>'.$fila['nombre'].'</td>';
                                  echo '<td>'.$fila['cantidad'].'</td>';
                                  echo '<td>'.$fila['longitud'].' '.$fila['unidad_medida'].'</td>';
                                  echo '<td>'.$fila['observacion'].'</td>';
                                  echo '<td>'.$fila['fecha_registro'].'</td>';
                                  echo '</tr>';
                          }
                          ?>             
                          </tbody>
                      </table>
                    </div>
                    <!---FIN TABLA -->
                  </div>
                </div>
              </div>
      </section>
    </div>
  </div>


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
    integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
  </script>
</body>

</html>