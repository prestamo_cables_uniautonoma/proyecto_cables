<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
    />
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
    />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="icon" href="img/logo.ico">
    <title>Login</title>
  </head>
  <body>


  <div class="main">
    <div class="container container-login">
      <div class="row">
        <div class="col">
          <center>
            <div class="middle">
                <div id="login">
                  <?php
                  include('control/validar.php');
                  ?>
                  <form action="control/validar.php" method="post">
                    <fieldset class="clearfix">
                      <div class="col-xl-7 offset-xl-1 col-lg-7 offset-lg-1">
                        <h1 class="title text-white">Prestamos Uniautonoma</h1>
                          <hr />
                          <p ><span class="fa fa-user"></span><input type="text" Placeholder="usuario" id="usuario" name="usuario" required></p>
                          <p><span class="fa fa-lock"></span><input type="password" id="contraseña" name="contraseña" Placeholder="contraseña" required></p>
                          <div>
                              <!--<span style="width:48%; text-align:left;  display: inline-block;"><a class="small-text" href="#">Forgotword?</a></span> -->
                              <span style="width:50%; text-align:right;  display: inline-block;"><input class="button-logout" type="submit" name="btn_ingresar" value="Iniciar Sesión"></span>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div> <!-- end login -->
            </div>
          </center>
        </div>
        <div class="col col-logo">
          <div class="logo">
            <img src="./img/logo.png" alt="">
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </body>
</html>



